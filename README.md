# LISTED COMPANIES CLIENT (REACT)

## INSTALLATION AND RUNNING INSTRUCTIONS

### API INSTALLATION AND RUNNING

Install and run [Listed Companies API](https://gitlab.com/vali-it-java/demo-projects/listed-companies-api) - the client app is dependent on it.

### BUILD

Run the following command in the project's root folder in order to build the project (install node and npm beforehand, if missing):
~~~
npm install
~~~

### RUN

Run the following command in order to start up the development server for serving the client app:

~~~
npm start
~~~

### PRODUCTION BULD

Run the following command in order to build the client for production:

~~~
npm run build
~~~