import React, { Component } from 'react';
import FetchApi from './FetchApi.js';
import CompanyListView from './CompanyListView.js';
import CompanyChartView from './CompanyChartView.js';
import CompanyEdit from './CompanyEdit.js';
import Popup, { POPUP_CONF_DEFAULT } from './Popup.js';
import Util from './Util.js';

export default class Companies extends Component {

    state = { companies: [], popupOpen: false, selectedCompanyId: 0, showChart: false };

    async componentDidMount() {
        this.loadCompanies();
    }

    loadCompanies = async () => {
        try {
            this.setState({ companies: await FetchApi.fetchCompanies() });
        } catch (e) {
            Util.handleError(e, this.props.onOpenLoginPopup);
        }
    }

    openPopup = companyId => this.setState({ popupOpen: true, selectedCompanyId: companyId });

    closePopup = () => this.setState({ popupOpen: false, selectedCompanyId: 0 });

    getSelectedCompany = () => {
        if (this.state.selectedCompanyId === 0) {
            return null;
        } else {
            return this.state.companies.find(c => c.id === this.state.selectedCompanyId);
        }
    }

    toggleShowChart = () => this.setState({ showChart: !this.state.showChart });

    render() {
        return (
            <React.Fragment>
                {this.state.companies.length > 0 &&
                    <React.Fragment>
                        <div className="items">
                            <div className="item-fluid">
                                <div className="item-100-center">
                                    <h1>Börsiettevõtted</h1>
                                </div>
                            </div>
                            <div className="item-fluid">
                                <div className="item-100-center">
                                    <button onClick={this.toggleShowChart}>
                                        {this.state.showChart ? 'Näita nimekirja' : 'Näita diagrammi'}
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!this.state.showChart &&
                            <CompanyListView
                                companies={this.state.companies}
                                onOpenPopup={this.openPopup}
                                onLoadCompanies={this.loadCompanies}
                                onOpenLoginPopup={this.props.onOpenLoginPopup}
                            />}
                        {this.state.showChart &&
                            <CompanyChartView
                                companies={this.state.companies}
                            />}
                    </React.Fragment>}
                {this.state.popupOpen &&
                    <Popup
                        conf={POPUP_CONF_DEFAULT}
                        onClose={this.closePopup}
                        content={
                            <CompanyEdit
                                company={this.getSelectedCompany()}
                                onClose={this.closePopup}
                                onLoadCompanies={this.loadCompanies}
                                onOpenLoginPopup={this.props.onOpenLoginPopup}
                            />
                        }
                    />}
            </React.Fragment>
        );
    }
}