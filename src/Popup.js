import React, { Component } from 'react';

export const POPUP_CONF_DEFAULT = {
    dimensionClass: 'popup-600-800',
    displayCloseButton: true,
    coverBackground: 'darkgrey',
    coverOpacity: '95%',
    coverCloseOnClick: true,
    width: '600px',
    height: '800px'
};

export const POPUP_CONF_BLANK_300_300 = {
    dimensionClass: 'popup-300-300',
    displayCloseButton: false,
    coverBackground: 'white',
    coverOpacity: '100%',
    coverCloseOnClick: false
};

export default class Popup extends Component {

    constructor(props) {
        super(props);
        this.popupWindow = React.createRef();
        this.popupBody = React.createRef();
        window.setTimeout(() => this.popupWindow.current.classList.add(this.props.conf.dimensionClass), 50);
        window.setTimeout(() => this.popupBody.current.style.display = "flex", 350);
    }

    render() {
        return (
            <React.Fragment>
                <div id="backgroundCover"
                    style={{
                        background: this.props.conf.coverBackground,
                        opacity: this.props.conf.coverOpacity
                    }}
                    onClick={this.props.conf.coverCloseOnClick ? this.props.onClose : null}
                >
                </div>
                <div id="popup"
                    style={{ 'top': (window.pageYOffset + 50) + 'px' }}
                    className={['popup'].join(' ')}
                    ref={this.popupWindow}>
                    <div id="popupContent">
                        {this.props.onClose &&
                            <div id="popupTopBar">
                                <svg height="20" width="20" onClick={this.props.onClose}>
                                    <line x1="0" y1="0" x2="20" y2="20" style={{ stroke: 'darkblue', strokeWidth: 2 }} />
                                    <line x1="0" y1="20" x2="20" y2="0" style={{ stroke: 'darkblue', strokeWidth: 2 }} />
                                </svg>
                            </div>}
                        <div id="popupBody" ref={this.popupBody}>
                            {this.props.content}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}