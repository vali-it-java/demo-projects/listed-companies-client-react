import React, { Component } from 'react';
import FetchApi from './FetchApi.js';
import Auth from './Auth.js';

export default class Login extends Component {

    state = {
        username: '',
        password: '',
    };

    login = async () => {
        try {
            if (this.state.username.trim().length > 0 && this.state.password.trim().length > 0) {
                let credentials = {
                    username: this.state.username,
                    password: this.state.password
                };
                const sesson = await FetchApi.login(credentials);
                Auth.storeAuthentication(sesson);
                this.props.onClose();
            }
        } catch (e) {
            console.log('Login failed', e);
        }
    }

    render() {
        return (
            <div id="loginFormContainer">
                <h2>Sisselogimine</h2>
                <div>
                    <label>Kasutajatunnus:</label>
                    <input type="text" onChange={event => this.setState({ username: event.target.value })} />
                    <label>Parool:</label>
                    <input type="password" onChange={event => this.setState({ password: event.target.value })} />
                    <button onClick={this.login}>Logi sisse</button>
                </div>
            </div>
        );
    }
}