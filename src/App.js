import React, { Component } from 'react';
import './App.css';
import './Popup.css';
import Header from './Header.js';
import Footer from './Footer.js';
import Companies from './Companies.js';
import Popup, { POPUP_CONF_BLANK_300_300 } from './Popup.js';
import Login from './Login';
import Auth from './Auth.js';

export default class App extends Component {

  state = { popupOpen: false };

  openLoginPopup = () => this.setState({ popupOpen: true });

  closeLoginPopup = () => this.setState({ popupOpen: false });

  render() {
    if (!this.state.popupOpen || Auth.getToken()) {
      return (
        <div id="wrapper">
          <Header onOpenLoginPopup={this.openLoginPopup} />
          <main>
            <Companies onOpenLoginPopup={this.openLoginPopup} />
          </main>
          <Footer />
        </div>
      );
    } else {
      return (
        <Popup
          conf={POPUP_CONF_BLANK_300_300}
          content={
            <Login
              onClose={this.closeLoginPopup}
            />
          }
        />
      );
    }
  }
}