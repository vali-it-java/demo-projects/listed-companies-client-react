import React from 'react';
import Company from './Company.js';

export default (props) => {
    return (
        <React.Fragment>
            <section className="items">
                {props.companies.map(
                    company => <Company
                        key={company.id}
                        company={company}
                        onOpenPopup={props.onOpenPopup}
                        onLoadCompanies={props.onLoadCompanies}
                        onOpenLoginPopup={props.onOpenLoginPopup} />
                )}
            </section>
            <div className="items">
                <div className="item-fluid">
                    <div className="item-100-center">
                        <button onClick={() => props.onOpenPopup(0)}>Lisa ettevõte</button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}