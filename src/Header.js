import React, { Component } from 'react';
import Auth from './Auth.js';

export default class Header extends Component {

    logout = () => {
        Auth.clearAuthentication();
        this.props.onOpenLoginPopup();
    }

    render() {
        return (
            <header>
                <div className="header-cell-left">
                    VALI-IT NÄIDISPROJEKT
                </div>
                {Auth.getToken() &&
                    <div className="header-cell-right">
                        <strong>{Auth.getUsername()}</strong> | <span onClick={this.logout}>logi välja</span>
                    </div>}
            </header>
        );
    }
}