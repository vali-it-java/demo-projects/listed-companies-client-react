import React, { Component } from 'react';
import Util from './Util.js';
import FetchApi from './FetchApi.js';

export default class Company extends Component {

    deleteCompany = async () => {
        if (window.confirm('Soovid sa tõesti seda ettevõtet kustutada?')) {
            try {
                await FetchApi.deleteCompany(this.props.company.id);
                this.props.onLoadCompanies();
            } catch (e) {
                Util.handleError(e, this.props.onOpenLoginPopup);
            }
        }
    }

    render() {
        return (
            <article className="item">
                <h2 className="item-100">{this.props.company.name}</h2>
                <div className="item-100"><img className="item-logo" src={this.props.company.logo} alt="Company logo" /></div>
                <div className="item-50-bold">Asutatud</div>
                <div className="item-50">{this.props.company.established}</div>
                <div className="item-50-bold">Töötajaid</div>
                <div className="item-50">{Util.formatNumber(this.props.company.employees)}</div>
                <div className="item-50-bold">Müügitulu</div>
                <div className="item-50">{Util.formatNumber(this.props.company.revenue)} &euro;</div>
                <div className="item-50-bold">Netotulu</div>
                <div className="item-50">{Util.formatNumber(this.props.company.netIncome)} &euro;</div>
                <div className="item-50-bold">Turuväärtus</div>
                <div className="item-50">{Util.formatNumber(parseInt(this.props.company.marketCapitalization))} &euro;</div>
                <div className="item-50-bold">Dividendimäär</div>
                <div className="item-50">{Util.formatNumber(this.props.company.dividendYield * 100)} %</div>
                <div className="item-100-center">
                    <button onClick={() => this.props.onOpenPopup(this.props.company.id)}>Muuda</button>
                    <button onClick={this.deleteCompany} className="button-red">Kustuta</button>
                </div>
            </article>
        );
    }
}