import React, { Component } from 'react';

export default class Footer extends Component {

    render() {
        return (
            <footer>
                &copy; Vali-IT 2020
            </footer>
        );
    }
}