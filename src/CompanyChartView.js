import React, { Component } from 'react';
import Chart from "chart.js";

export default class CompanyChartView extends Component {

    chartRef = React.createRef();

    generateRandomColor() {
        let r = Math.floor(Math.random() * 255);
        let g = Math.floor(Math.random() * 255);
        let b = Math.floor(Math.random() * 255);
        return `rgb(${r},${g},${b})`;
    }
    
    composeChartDataset(items) {
        if (items != null && items.length > 0) {
            let data = items.map(item => Math.round(item.marketCapitalization));
            let backgroundColors = items.map(this.generateRandomColor);
            return [{
                data: data,
                backgroundColor: backgroundColors,
            }];
        }
        return [];
    }
    
    composeChartLabels(items) {
        return items != null && items.length > 0 ? items.map(item => item.name) : [];
    }
    
    composeChartData(companies) {
        return {
            datasets: this.composeChartDataset(companies),
            labels: this.composeChartLabels(companies)
        };
    }

    componentDidMount() {
        const ctx = this.chartRef.current.getContext("2d");
        let chartData = this.composeChartData(this.props.companies);
        new Chart(ctx, {
            type: 'pie',
            data: chartData,
            options: {}
        });
    }

    render() {
        return (
            <div className="item-fluid">
                <canvas ref={this.chartRef} style={{ height: '40vh', width: '80%' }}></canvas>
            </div>
        );
    }
}