import React, { Component } from 'react';
import FetchApi from './FetchApi.js';
import Util from './Util.js';

export default class CompanyEdit extends Component {

    state = {
        errors: [],
        companyId: this.props.company ? this.props.company.id : 0,
        companyName: this.props.company ? this.props.company.name : '',
        companyLogo: this.props.company ? this.props.company.logo : '',
        companyEstablished: this.props.company ? this.props.company.established : '',
        companyEmployees: this.props.company ? this.props.company.employees : '',
        companyRevenue: this.props.company ? this.props.company.revenue : '',
        companyNetIncome: this.props.company ? this.props.company.netIncome : '',
        companySecurities: this.props.company ? this.props.company.securities : '',
        companySecurityPrice: this.props.company ? this.props.company.securityPrice : '',
        companyDividends: this.props.company ? this.props.company.dividends : ''
    };

    constructor(props) {
        super(props);
        this.companyLogo = React.createRef();
        this.companyEditContainer = React.createRef();
    }

    saveCompany = async () => {
        try {
            let errors = this.validateCompany();
            if (errors.length === 0) {
                let company = {
                    id: this.state.companyId,
                    name: this.state.companyName,
                    logo: this.state.companyLogo,
                    established: this.state.companyEstablished,
                    employees: this.state.companyEmployees,
                    revenue: this.state.companyRevenue,
                    netIncome: this.state.companyNetIncome,
                    securities: this.state.companySecurities,
                    securityPrice: this.state.companySecurityPrice,
                    dividends: this.state.companyDividends
                };
                await FetchApi.postCompany(company);
                this.props.onClose();
                this.props.onLoadCompanies();
            } else {
                this.setState(
                    { errors: errors },
                    () => this.companyEditContainer.current.scrollTop = 0
                );
            }
        } catch (e) {
            Util.handleError(e, this.props.onOpenLoginPopup);
        }
    }

    validateCompany = () => {
        let errors = [];

        if (!this.state.companyName || this.state.companyName.length < 2) {
            errors.push('Ebakorrektne nimi (nõutud vähemalt kaks tähemärki)!');
        }

        if (!this.state.companyEstablished) {
            errors.push('Ettevõtte asutamisaeg määramata!');
        }

        if (new Date(this.state.companyEstablished) > new Date()) {
            errors.push('Ettevõtte asutamisaeg ei saa olla tulevikus!');
        }

        return errors;
    }

    uploadFile = async () => {
        try {
            const logoFile = this.companyLogo.current.files[0];
            if (logoFile) {
                let uploadResponse = await FetchApi.postFile(logoFile);
                this.setState({ companyLogo: uploadResponse.url });
            }
        } catch (e) {
            Util.handleError(e, this.props.onOpenLoginPopup);
        }
    }

    render() {
        return (
            <div id="itemEditFormContainer" ref={this.companyEditContainer}>
                <h2>{this.props.company ? 'Ettevõtte muutmine' : 'Ettevõtte lisamine'}</h2>
                <input type="hidden" id="itemEditId" />
                {
                    this.state.errors.length > 0 &&
                    <div id="errorBox">{this.state.errors.map(e => (<div key={e}>{e}</div>))}</div>
                }
                <div>
                    <label>Nimi</label>
                    <input
                        type="text"
                        value={this.state.companyName}
                        onChange={event => this.setState({ companyName: event.target.value })} />
                    {
                        this.state.companyLogo &&
                        <div>
                            <img alt="Logo" id="itemEditLogoImage" src={this.state.companyLogo} />
                        </div>
                    }

                    <label>Logo</label>
                    <input
                        type="text"
                        value={this.state.companyLogo}
                        onChange={event => this.setState({ companyLogo: event.target.value })} />
                    <input
                        type="file"
                        title="Lae logo fail"
                        ref={this.companyLogo} />
                    <div>
                        <button onClick={this.uploadFile}>Lae fail</button>
                    </div>

                    <label>Asutatud</label>
                    <input
                        type="date"
                        value={this.state.companyEstablished}
                        onChange={event => this.setState({ companyEstablished: event.target.value })} />

                    <label>Töötajaid</label>
                    <input
                        type="number"
                        min="0" max="1000000" step="1"
                        value={this.state.companyEmployees}
                        onChange={event => this.setState({ companyEmployees: event.target.value })} />

                    <label>Müügitulu (&euro;)</label>
                    <input
                        type="number"
                        min="-1000000000000" max="1000000000000" step="1"
                        value={this.state.companyRevenue}
                        onChange={event => this.setState({ companyRevenue: event.target.value })} />

                    <label>Netotulu (&euro;)</label>
                    <input
                        type="number"
                        min="-1000000000000" max="1000000000000" step="1"
                        value={this.state.companyNetIncome}
                        onChange={event => this.setState({ companyNetIncome: event.target.value })} />

                    <label>Aktsiate arv</label>
                    <input
                        type="number"
                        min="0" max="1000000000000" step="1"
                        value={this.state.companySecurities}
                        onChange={event => this.setState({ companySecurities: event.target.value })} />

                    <label>Aktsia hind (&euro;)</label>
                    <input
                        type="number"
                        step="0.01"
                        value={this.state.companySecurityPrice}
                        onChange={event => this.setState({ companySecurityPrice: event.target.value })} />

                    <label>Dividende aktsia kohta (&euro;)</label>
                    <input
                        type="number"
                        step="0.01"
                        value={this.state.companyDividends}
                        onChange={event => this.setState({ companyDividends: event.target.value })} />

                    <button onClick={this.saveCompany}>Salvesta</button>
                </div>
            </div>
        );
    }
}